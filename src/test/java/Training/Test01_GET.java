package Training;

import static io.restassured.RestAssured.*;
//import io.restassured.RestAssured;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;

import io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class Test01_GET {

    @Test
    void test01() {
        //Response response = RestAssured.get("https://reqres.in/api/users?page=2");
        Response response = get("https://reqres.in/api/users?page=2");
        System.out.println(response.getBody().toString());
        System.out.println(response.getStatusCode());
        System.out.println(response.asString());
        System.out.println(response.getContentType());
        System.out.println(response.getTime());

        Assert.assertEquals(response.getStatusCode(), 200);
    }
    @Test
    void test02() {
        given().
                get("https://reqres.in/api/users?page=2")
                .then()
                .statusCode(200)
                .body("data.id[1]", equalTo(8));

        given().
                get("https://reqres.in/api/users?page=2")
                .then()
                .body("page", equalTo(2))
                .body("total", equalTo(12));

    }
    @Test
    public void test03() {
        given().
                get("https://reqres.in/api/users?page=2")
                .then()
                .body("data[0].last_name", equalTo("Lawson"));

        given().
                get("https://reqres.in/api/users?page=2")
                .then()
                .body("data[0].id", equalTo(7));


        String supportText = "To keep ReqRes free, contributions towards server costs are appreciated!";
        given().
                get("https://reqres.in/api/users?page=2")
                .then()
                .body("support.text", equalTo(supportText));
    }
}


