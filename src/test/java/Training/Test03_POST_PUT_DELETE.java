package Training;

import io.restassured.http.ContentType;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
//https://github.com/rest-assured/rest-assured/wiki/GettingStarted

public class Test03_POST_PUT_DELETE {

    @Test
    public void test01() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("name", "Raghav");
        map.put("job", "Teacher");
        System.out.println(map);

        JSONObject request = new JSONObject(map); //dependency: json-simple

        System.out.println(request);
        System.out.println(request.toJSONString());

        given()
                .header("Contenet-type", "application/json")
                .contentType(ContentType.JSON).accept(ContentType.JSON)
                .body(request.toJSONString()).
                when().
                post("https://reqres.in/api/users")
                .then()
                .statusCode(201)
                .log().all();
    }

    @Test// druga opcja bez wykorzystania MAP
    public void test02() {
        JSONObject request = new JSONObject(); //dependancy: json-simple

        request.put("name", "Raghav");
        request.put("job", "Teacher");

        System.out.println(request);
        System.out.println(request.toJSONString());

        given()
                .header("Contenet-type", "application/json")
                .contentType(ContentType.JSON).accept(ContentType.JSON)
                .body(request.toJSONString()).
                when().
                post("https://reqres.in/api/users")
                .then()
                .statusCode(201)
                .log().body();

    }

    @Test//PUT
    public void test03() {
        JSONObject request = new JSONObject(); //dependancy: json-simple

        request.put("name", "RaghavXD");
        request.put("job", "TeacherXD");

        System.out.println(request);
        System.out.println(request.toJSONString());

        given()
                .header("Contenet-type", "application/json")
                .contentType(ContentType.JSON).accept(ContentType.JSON)
                .body(request.toJSONString()).
                when().
                put("https://reqres.in/api/users/2")
                .then()
                .statusCode(200)
                .log().body();
    }

    @Test//DELETE
    public void test04() {

                when().
                delete("https://reqres.in/api/users/2")
                .then()
                .statusCode(204)
                .log().all();
    }

}
