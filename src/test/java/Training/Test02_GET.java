package Training;

import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
//https://github.com/rest-assured/rest-assured/wiki/GettingStarted

public class Test02_GET {

    @Test
    public void test_01() {

        given().
                get("https://reqres.in/api/users?page=2")
                .then()
                .statusCode(200).log().all();
    }

    @Test
    public void test_02() {

        given().
                get("https://reqres.in/api/users?page=2")
                .then()
                .body("data[0].id", equalTo(7))
                .body("data.first_name", hasItem("Michael")).log().all()
                .body("data.first_name", hasItems("Michael", "Lindsay"));
    }
    @Test
    public void test_03() {

        given().
                header("Content-type", "application/json")
                .get("https://reqres.in/api/users?page=2")
                .then()
                .body("data[2].first_name", equalTo("Tobias"));
    }
}
