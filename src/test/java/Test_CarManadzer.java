import io.restassured.http.ContentType;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
//https://github.com/rest-assured/rest-assured/wiki/GettingStarted

import static org.hamcrest.Matchers.*;

public class Test_CarManadzer {

    @Test//POST
    public void firstTest() {
        JSONObject request = new JSONObject(); //dependancy: json-simple
        request.put("mark","Renault");
        request.put("model","Espace");
        request.put("vin",12355444);
        request.put("power",1000);

        given().
                header("Content-type","application-json")
                .contentType(ContentType.JSON).accept(ContentType.JSON)
                .body(request.toJSONString())
                .when()
                .post("http://localhost:8080/api/db/addNewCar")
                .then()
                .statusCode(201)
                .log().all();
    }
    @Test//GET
    public void secondTest() {

        given()
                .get("http://localhost:8080/api/db/carList")
                .then()
                .statusCode(200)
                .body("mark[0]", equalTo("BMW"))
                .body("model[1]", equalTo("Q8"))
                .log().body();
    }

    @Test//GET
    public void thirdTest() {
        given()
                .header("Content-Type", "application/json")
                .get("http://localhost:8080/api/db/car/2")
                .then()
                .body("mark", equalTo("Audi"))
                .body("model", equalTo("Q8"))
                .body("vin", equalTo(222222222))
                .body("power", equalTo(470))
                .statusCode(200)
                .log().body()
                .log().headers();

    }

    @Test//delete
    public void fourthTest() {

        given()
                .header("Contnt-Type","application/json")
                .contentType(ContentType.JSON).accept(ContentType.JSON)
                .delete("http://localhost:8080/api/db/deleteCar/25")//numer rekordu z bazy
                .then()
                .log().all();
    }


}
